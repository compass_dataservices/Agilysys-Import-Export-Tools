#! /usr/bin/python3

import json
import os
import requests
import time
from datetime import datetime
from configparser import ConfigParser

UPDATE_URL = 'https://bitbucket.org/api/2.0/repositories/compass_dataservices/agilysys-import-export-tools/downloads'
APP_DIR = os.path.join(os.getenv('APPDATA'), 'Agilysys Format Tools')
CONFIG_FILE = os.path.join(APP_DIR, 'config.ini')
LOG_FILE = os.path.join(APP_DIR, 'errors.log')
CONFIG = ConfigParser()
CONFIG.read(CONFIG_FILE)


def check_for_update(update_url):
    response = requests.get(update_url)
    data = response.json()
    last_update_date = data['values'][0]['created_on']
    update_name = data['values'][0]['name']
    download_link = data['values'][0]['links']['self']['href']

    last_update_time = parse_datetime(last_update_date)

    try:
        current_install_date_string = CONFIG['Update Info']['current install date']
        current_install_date = parse_datetime(current_install_date_string)
    except KeyError:
        current_install_date = datetime.utcnow()
        # '2016-03-21T18:01:04.824811+00:00'
        install_date_string = build_date_string(current_install_date)
        CONFIG['Update Info'] = {'current install date': install_date_string}
        with open(CONFIG_FILE, 'w') as f:
            CONFIG.write(f)

    print('Most recent update published on {}.\n{}\n'.format(last_update_date, last_update_time))
    print('Current version installed on {}'.format(current_install_date))

    if current_install_date < last_update_time:
        print('new update available')
        print('Download: {0} \nLink: {1}'.format(update_name, download_link))
        return download_link, update_name
    else:
        print('running latest version')
        return 'n/a', 'current'


def build_date_string(date):
    date_string = '{:0>2d}-{:0>2d}-{:0>2d}T{:0>2d}:{:0>2d}:{:0>2d}'.format(
        date.year, date.month, date.day, date.hour, date.minute,date.second)
    return date_string


def parse_datetime(date_string):
    last_update_year = int(date_string[:4])
    last_update_month = int(date_string[5:7])
    last_update_day = int(date_string[8:10])
    last_update_hour = int(date_string[11:13])
    last_update_minute = int(date_string[14:16])
    last_update_time = datetime(last_update_year,
                                last_update_month, last_update_day, last_update_hour, last_update_minute)
    return last_update_time


def download_update(url, name):
    update = requests.get(url)
    with open(name, 'wb+') as file:
        file.write(update.content)

    CONFIG['Update Info'] = {'current install date': build_date_string(datetime.utcnow())}
    with open(CONFIG_FILE, 'w') as f:
        CONFIG.write(f)

    print('Update downloaded successfully')


def main():
    global dl_link, dl_name
    dl_link, dl_name = check_for_update(UPDATE_URL)


if __name__ == "__main__":
    main()
